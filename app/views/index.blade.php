<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        {{ HTML::style('assets/css/styles.css') }}
    </head>
    <body>
        <div class="row" id="begin">
            <div class="col-sm-10 col-sm-offset-1">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="name">Product name</label>
                            <input type="text" class="form-control" id="name" placeholder="Product name">
                        </div>
                        <div class="form-group">
                            <label for="quantity">Quantity in stock</label>
                            <input type="number" class="form-control" id="quantity" placeholder="Quantity in stock">
                        </div>
                        <div class="form-group">
                            <label for="price">Price per item</label>
                            <input type="number" class="form-control" id="price" placeholder="Price per item">
                        </div>
                        <button type="submit" class="btn btn-default btn-success" id="sending">Send</button>
                    </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Quantity in stock</th>
                        <th>Price per item</th>
                        <th>Datetime submitted</th>
                        <th>Total value number</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="items">

                    </tbody>
                </table>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0=" crossorigin="anonymous"></script>
        {{ HTML::script('assets/js/app.js') }}

    </body>
</html>