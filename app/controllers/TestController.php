<?php

class TestController extends \BaseController {

	public function store(){
		$input = json_decode(Input::get('data'));

		$item = new Item();
		$item->name = $input->name;
		$item->quantity = $input->quantity;
		$item->price = $input->price;
		$item->save();

		return [true];

	}

	public function load(){
		$items = Item::all();
		$itemArray = [];

		if(count($items) >0 ){
			$date = new DateTime();
			foreach ($items as $item){

				$i = [
					'id' => $item->id,
					'name' => $item->name,
					'quantity' => $item->quantity,
					'price' => $item->price,
					'timeCreated' =>  $item->created_at ,
					'totalValue' => $item->price * $item->quantity,
				];

				array_push($itemArray,$i);
			}

			return $itemArray;
		}
	}

}