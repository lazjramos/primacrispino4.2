/**
 * Created by Laz J. Ramos on 05-Jul-16.
 */
$(document).ready(function () {
    loading();
});

$('#sending').click(function (e) {
    e.preventDefault();

    myArray = {
        "name" : $('#name').val(),
        "quantity" : $('#quantity').val(),
        "price" : $('#price').val(),
    }

    console.log(myArray);
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        url: "store",
        data: "data="+JSON.stringify(myArray),
        timeout: 10000,
        success: function (answer) {
            console.log(answer)
            if (answer[0] == true){
                loading();
            }
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });

});

function loading() {
    $.ajax({
        async: true,
        type: "POST",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        url: "load",
        // data: "data="+JSON.stringify(myArray),
        timeout: 10000,
        success: function (answer) {
            console.log(answer);

            tbody = $('#items');
            tbody.empty();

            // if (answer.length > 0){
                $.each(answer,function (index, obj) {
                    tbody.append('' +
                        '<tr>' +
                        '<td>'+ obj.name+'</td>' +
                        '<td>'+ obj.quantity+'</td>' +
                        '<td>'+ obj.price+'</td>' +
                        '<td>'+ obj.timeCreated.date+'</td>' +
                        '<td>'+ obj.totalValue +'</td>' +
                        '<td><a href="'+ obj.id +'">Edit</a></td>' +
                        '</tr>'
                        +'')
                });
            // }
        },
        error: function (MLHttpRequest, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}
